﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_ISOB_
{
    class Client
    {
        private string id;
        private string key;
        private string key_c_tgs;
        private string key_c_ss;
        private int t4;
        private Des.des des;
        public Client(string id, string key)
        {
            this.id = id;
            this.key = key;
            key_c_tgs = "";
            key_c_ss = "";
            t4 = 0;
            des = new Des.des();
        }

        public string SendToAS()
        {
            Servers.Logger.Logging("message sent to as server");
            Servers.Logger.Logging(id);
            return id;
        }

        public string SendToTGS(string message)
        {
            List<string> response_from_as = des.Decrypt(message, key).Split(' ').ToList();
            string tgt = response_from_as[0];
            key_c_tgs = response_from_as[1];

            DateTime dt = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan tsInterval = dt.Subtract(dt1970);
            Int32 iSeconds = Convert.ToInt32(tsInterval.TotalSeconds);

            string aut1 = id + ' ' + iSeconds.ToString();
            string response = tgt + ' ' + des.Encrypt(aut1, key_c_tgs) + ' ' + "ss01";

            Servers.Logger.Logging("message sent to tgs server");
            Servers.Logger.Logging(response);
            return response;
        }

        public string SendToSS(string message)
        {
            List<string> response_from_tgs = des.Decrypt(message, key_c_tgs).Split(' ').ToList();
            key_c_ss = response_from_tgs[1];

            DateTime dt = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan tsInterval = dt.Subtract(dt1970);
            Int32 iSeconds = Convert.ToInt32(tsInterval.TotalSeconds);

            t4 = iSeconds;
            string aut2 = id + ' ' + t4.ToString();
            string response = response_from_tgs[0] + ' ' + des.Encrypt(aut2, key_c_ss);

            Servers.Logger.Logging("message sent to ss server");
            Servers.Logger.Logging(response);
            return response;
        }

        public bool ValidateSS(string message)
        {
            message = des.Decrypt(message, key_c_ss);
            string mes = "";
            foreach (var t in message)
                if (t != '\0')
                    mes += t;
            if (t4 != Convert.ToInt32(mes) - 1)
                throw new Exception("server verification fail");

            Servers.Logger.Logging("ss server verificated by client");
            return true;
        }
    }
}
