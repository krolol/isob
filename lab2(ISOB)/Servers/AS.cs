﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_ISOB_.Servers
{
    class AS
    {
        private Dictionary<string, string> clients;
        private Des.des des;
        private Dictionary<string, string> tgs_servers;
        public AS()
        {
            clients = new Dictionary<string, string>();
            tgs_servers = new Dictionary<string, string>();
            des = new Des.des();
        }

        public void RegisterClient(string key, string value)
        {
            clients[key] = value;
        }

        public void AddTGS(string id, string key)
        {
            tgs_servers[id] = key;
        }

        public string Response(string id)
        {
            if (!clients.Keys.Contains(id))
                throw new Exception("client not found");

            Random rand = new Random();
            string key_c_tgs = Convert.ToString(rand.Next(1, 100000));
            int timespan = 60;
            DateTime dt = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan tsInterval = dt.Subtract(dt1970);
            Int32 iSeconds = Convert.ToInt32(tsInterval.TotalSeconds);

            string tgt = id + ' ' + tgs_servers.Keys.ToList()[0] + ' ' + iSeconds.ToString()+ ' ' + timespan.ToString() + ' ' + key_c_tgs;
            string response = des.Encrypt(tgt, tgs_servers.Values.ToList()[0]) + ' ' + key_c_tgs;

            Logger.Logging("AS server responsed to client");
            Logger.Logging(des.Encrypt(response, clients[id]));

            return des.Encrypt(response, clients[id]);

        }
    }
}
