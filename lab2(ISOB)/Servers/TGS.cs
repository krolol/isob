﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_ISOB_.Servers
{
    class TGS
    {
        private Des.des des;
        private string id;
        private string key_as_tgs;
        private Dictionary<string, string> ss_servers;
        public TGS(string id, string key)
        {
            this.id = id;
            key_as_tgs = key;
            ss_servers = new Dictionary<string, string>();
            des = new Des.des();
        }

        public void Add_SS(string id, string key)
        {
            ss_servers[id] = key;
        }

        public string Response(string message)
        {
            List<string> from_client = message.Split(' ').ToList();
            List<string> tgt = des.Decrypt(from_client[0], key_as_tgs).Split(' ').ToList();
            string key_c_tgs = tgt[4];
            List<string> aut1 = des.Decrypt(from_client[1], key_c_tgs).Split(' ').ToList();
            string ss_id = from_client[2];
            
            if (!tgt[0].Contains(aut1[0]) || tgt[1] != id || Convert.ToInt32(aut1[1]) - Convert.ToInt32(tgt[2]) > Convert.ToInt32(tgt[3]))
                throw new Exception("verification fail");
            Random rand = new Random();
            int key_c_ss = rand.Next(1, 100000);
            DateTime dt = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);
            DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan tsInterval = dt.Subtract(dt1970);
            Int32 iSeconds = Convert.ToInt32(tsInterval.TotalSeconds);
            string tgs = tgt[0] + ' ' + from_client[2] + ' ' + iSeconds.ToString() + ' ' + "100" + ' ' + key_c_ss.ToString();
            string response = des.Encrypt(des.Encrypt(tgs, ss_servers[ss_id]) + ' ' + key_c_ss.ToString(), key_c_tgs);
            Logger.Logging("tgs server responsed client");
            Logger.Logging(response);
            return response;
        }
    }
}
