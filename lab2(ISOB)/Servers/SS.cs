﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2_ISOB_.Servers
{
    class SS
    {
        private Des.des des;
        private string id;
        private string key_tgs_ss;
        private Dictionary<string, string> clients;
        public SS(string id, string key)
        {
            des = new Des.des();
            this.id = id;
            key_tgs_ss = key;
            clients = new Dictionary<string, string>();
        }

        public string Response(string message)
        {
            List<string> mes = message.Split(' ').ToList();
            List<string> tgs = des.Decrypt(mes[0], key_tgs_ss).Split(' ').ToList();
            string key_c_ss = tgs[4];
            string client_id = tgs[0];

            List<string> aut2 = des.Decrypt(mes[1], key_c_ss).Split(' ').ToList();
            if (!tgs[0].Contains(aut2[0]) || Convert.ToInt64(aut2[1]) - Convert.ToInt64(tgs[2]) > Convert.ToInt64(tgs[3]))
                throw new Exception("verification fail");

            clients[client_id] = key_c_ss;
            Logger.Logging("ss server responsed");
            Logger.Logging(des.Encrypt(Convert.ToString(Convert.ToInt32(aut2[1]) + 1), key_c_ss));
            return des.Encrypt(Convert.ToString(Convert.ToInt32(aut2[1]) + 1), key_c_ss);
        }
    }
}
