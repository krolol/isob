﻿using System;
using lab2_ISOB_.Des;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2_ISOB_
{
    public partial class Form1 : Form
    {
        private string tgs_id = "tgs001";
        private string tgs_pswd = "tgs1234";
        private string ss_id = "ss01";
        private string ss_pswd = "ss4321";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string client_id = textBox1.Text;
            string client_pswd = textBox2.Text;
            Servers.AS as_server = new Servers.AS();
            Client client = new Client(client_id, client_pswd);
            as_server.RegisterClient(client_id, client_pswd);

            Servers.TGS tgs = new Servers.TGS(tgs_id, tgs_pswd);
            as_server.AddTGS(tgs_id, tgs_pswd);

            Servers.SS ss = new Servers.SS(ss_id, ss_pswd);
            tgs.Add_SS(ss_id, ss_pswd);

            string buffer = "";
            try
            {
                buffer = client.SendToAS();
                buffer = as_server.Response(buffer);
                buffer = client.SendToTGS(buffer);
                buffer = tgs.Response(buffer);
                buffer = client.SendToSS(buffer);
                buffer = ss.Response(buffer);
                bool res = client.ValidateSS(buffer);
                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                buffer = "";
            }
        }
    }
}
