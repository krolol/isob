﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2_ISOB_.Des
{
    class des
    {
        private List<string> keys = new List<string>();

        private string StringToBinary(string data)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in data.ToCharArray())
            {
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
            }
            return sb.ToString();
        }

        private string BinaryToString(string data)
        {
            string res = "";

            for (int i = 0; i < data.Length; i += 8)
            {
                res += (char)(Convert.ToByte(data.Substring(i, 8), 2));
            }
            return res;
        }

        //Первичная перестановка текста по таблице table
        private string InitPermText(string text, List<int> table)
        {
            string new_text = "";
            foreach (var t in table)
            {
                new_text += text[t - 1];
            }
            return new_text;
        }

        //Перестановка наоборот
        private string PermText(string text, List<int> table)
        {
            StringBuilder new_text = new StringBuilder(text);
            int i = 0;
            foreach (var t in table)
            {
                new_text[t - 1] = text[i];
                i++;
            }
            return new_text.ToString();
        }

        private void EncryptKey(string key)
        {
            keys.Clear();
            //перестановка по таблице pc_1(56-bit)
            key = InitPermText(key, tables.pc_1);
            string c0 = key.Substring(0, key.Length / 2);
            string d0 = key.Substring(key.Length / 2, key.Length / 2);
            //16 операций
            foreach(var t in tables.shifts)
            {
                //16 смещений по таблице shifts 
                c0 = LeftShift(c0, t);
                d0 = LeftShift(d0, t);
                //16 перестановок по таблице pc_2(48-bit)
                keys.Add(InitPermText(c0 + d0, tables.pc_2));
            }
        }

        private string RightLength(string text)
        {
            string res = StringToBinary(text);
            while (res.Length % 64 != 0)
                res = "0" + res;
            return res;
        }

        public string Encrypt(string message, string key)
        {
            string res = "";
            key = RightLength(key);
            message = RightLength(message);
            key = key.Substring(0, 64);
            for (int i = 0; i < message.Length / 64; i++)
            {
                res += EncryptText(message.Substring(i * 64, 64), key);
            }
            return res;
        }

        public string Decrypt(string message, string key)
        {
            string res = "";
            key = RightLength(key);
            message = RightLength(message);
            key = key.Substring(0, 64);
            for (int i = 0; i < message.Length / 64; i++)
            {
                res += DecryptText(message.Substring(i * 64, 64), key);
            }
            return res;
        }

        private string EncryptText(string message, string key)
        {
            EncryptKey(key);
            //перестановка текста по таблице 
            message = InitPermText(message, tables.ip);
            //делим пополам и вычисляем функцию f(16 раз)
            string l0 = message.Substring(0, message.Length / 2);
            string r0 = message.Substring(message.Length/2, message.Length/2);
            for (int i = 0; i < 16; ++i)
            {
                string temp = l0;
                l0 = r0;
                r0 = XOR(temp, fFunc(r0, keys[i]));
            }
            message = r0 + l0;
            //Перестановка по таблице ip_1
            message = InitPermText(message ,tables.ip_1);
            message = BinaryToString(message);
            return message;
        }

        private string DecryptText(string message, string key)
        {
            EncryptKey(key);
            message = PermText(message, tables.ip_1);
            string l16 = message.Substring(message.Length / 2, message.Length / 2);
            string r16 = message.Substring(0, message.Length / 2);
            for (int i = 0; i < 16; i++)
            {
                string temp = r16;
                r16 = l16;
                l16 = fFunc(r16, keys[keys.Count - i - 1]);
                l16 = XOR(temp, l16);
            }
            message = l16 + r16;
            message = PermText(message, tables.ip);
            message = BinaryToString(message);
            return message;

        }

        //Сдвиг половинок влево на number
        private string LeftShift(string key, int number)
        {
            for (int i = 0; i < number; i++)
            {
                var n_key = key.ToCharArray().ToList();
                var temp = n_key[0];
                n_key.Remove(temp);
                n_key.Add(temp);
                key = "";
                foreach (var t in n_key)
                    key += t;
            }
            return key;
        }

        //функция f
        private string fFunc(string r, string key)
        {
            //Перестановка по таблице e_bit
            r = InitPermText(r, tables.e_bit);
            //xor key и E(R)
            string xor = XOR(key, r);
            string result = "";
            for (int i = 0; i < 8; ++i)
            {
                //S1(b1)S2(b2)..S8(b8)
                string b = xor.Substring(i*xor.Length/8, xor.Length/8);
                result += PermS(b, s_tables.s[i]);
            }
            //Перестановка по таблице p
            result = InitPermText(result, tables.p);
            return result;

        }

        //xor для string
        private string XOR(string s1, string s2)
        {
            string result = "";

            for (int i = 0; i < s1.Length; i++)
            {
                bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
                bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

                if (a ^ b)
                    result += "1";
                else
                    result += "0";
            }
            return result;
        }

        //Перестановка по s
        private string PermS(string text, List<int> list)
        {
            int row = Convert.ToInt32(text[0].ToString() + text[text.Length - 1].ToString(), 2);
            int column = Convert.ToInt32(text[1].ToString() + text[2].ToString() + text[3].ToString() + text[4].ToString(), 2);
            int numb = row * 16 + column;
            int res_numb = list[numb];
            string result = Convert.ToString(res_numb, 2);
            while (result.Length != 4)
                result = "0" + result; 
            return result;
        }
    }
}
